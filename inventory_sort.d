import core.stdc.config;
import core.thread.osthread;
import core.time;

import std.algorithm.comparison;
import std.algorithm.iteration;
import std.algorithm.mutation;
import std.algorithm.searching;
import std.algorithm.sorting;
import std.array;
import std.conv;
import std.datetime.systime;
import std.exception;
import std.file;
import std.math;
import std.process;
import std.random;
import std.range;
import std.stdio;
import std.string;

import deimos.X11.X;
import deimos.X11.Xlib;
import deimos.X11.keysym;

import ae.sys.file;
import ae.sys.sendinput;
import ae.utils.array;
import ae.utils.geometry;
import ae.utils.graphics.color;
import ae.utils.graphics.hls;
import ae.utils.graphics.im_convert;
import ae.utils.graphics.image;
import ae.utils.graphics.view;
import ae.utils.math;
import ae.utils.meta;

pragma(lib, "X11");
pragma(lib, "Xtst");

// ---------------------------------------------------------------------------------------------------------------------

enum gameTitle = "The Outer Worlds  ";

// ---------------------------------------------------------------------------------------------------------------------

Display* dpy;
Window win;
Rect!int geometry;

Image!BGRA getCapture()
{
	static Image!BGRA capture;
	captureWindowRect(win, Rect!int(0, 0, geometry.w, geometry.h), capture);
	// capture.colorMap!(c => RGBA(c.r, c.g, c.b, c.a)).toPNG.toFile("test.png");
	return capture;
}

extern (C) int XTestFakeKeyEvent(
    Display*		/* dpy */,
    uint	/* keycode */,
    Bool		/* is_press */,
    c_ulong	/* delay */
);
extern (C) int XTestFakeButtonEvent(
	Display *display,
	uint button,
	Bool is_press, c_ulong delay
);

void moveMouse(int x, int y)
{
	XWarpPointer(dpy, None, win, 0, 0, 0, 0, x, y);
	XSync(dpy, false);
}

void sendButton(int button, bool press)
{
	// XEvent ev2;
	// ev2.type = press ? ButtonPress : ButtonRelease;
	// ev2.xbutton.button = button;
	// ev2.xbutton.same_screen = True;
	// XSendEvent(dpy, win, True/*propagate*/, press ? ButtonPressMask : ButtonReleaseMask, &ev2);
	XTestFakeButtonEvent(dpy, button, press, 1);
	XSync(dpy, false);
}

void sendScroll(int dir)
{
	auto button = dir < 0 ? Button4 : Button5;
	sendButton(button, true);
	Thread.sleep(50.msecs);
	sendButton(button, false);
}

void sendDoubleClick()
{
	foreach (n; 0 .. 2)
	{
		sendButton(Button1, true);
		Thread.sleep(30.msecs);
		sendButton(Button1, false);
		Thread.sleep(30.msecs);
	}
}

// ---------------------------------------------------------------------------------------------------------------------

Rect!xy_t findItemFrame(V)(V i)
{
	xy_t[] doAxis(int thresholdLow, int thresholdHigh, int minL, V)(V i)
	{
		xy_t[] results;
		bool inRegion;
		xy_t bestX, bestL;

		foreach (x; 0 .. i.w)
		{
			xy_t al;
			{
				xy_t start = 0;
				foreach (y; 0 .. i.h)
				{
					auto c = i[x, y];
					HLS!BGRA hls;
					ushort h, l, s;
					hls.toHLS(c, h, l, s);

					if (l > minL)
						al = max(al, y - start);
					else
						start = y;
				}
			}

			if (!inRegion)
			{
				if (al >= thresholdLow)
				{
					inRegion = true;
					debug(item_frame) writeln("Entering region at ", x);
				}
			}
			else
			{
				if (al > bestL)
				{
					bestL = al;
					bestX = x;
				}
				if (al < thresholdLow)
				{
					debug(item_frame) writeln("Exiting region - bestL = ", bestL, " at ", bestX);
					if (bestL >= thresholdHigh)
						results ~= bestX;
					bestL = 0;
					inRegion = false;
				}
			}
		}
		return results;
	}

	auto resultsX = doAxis!(100, 350, 120)(i);
	debug(item_frame) writeln("-------------------------");
	auto resultsY = doAxis!(100, 500, 110)(i.flipXY);

	if (resultsX.length == 0 && resultsY.length == 0)
		return typeof(return).init;

	if (resultsX.length == 3 && resultsX[$-2] + 11 == resultsX[$-1])
		resultsX = resultsX[0 .. $-1];

	enforce(resultsX.length == 2, "Vertical frame not found - %s".format(resultsX));
	enforce(resultsY.length == 2, "Horizontal frame not found - %s".format(resultsY));
		// return results[0 .. 2];

	return Rect!xy_t(resultsX[0], resultsY[0], resultsX[1], resultsY[1]);
}

// bool isTextColor(COLOR)(COLOR c) { return c.r == 0xff && c.g == 0xf2 && c.b == 0xc7; }
bool isTextColor(COLOR)(COLOR c) { return c.r >= 0xef && c.g >= 0xe2 && c.b >= 0xba; }

bool startsWithWord(string s, string word) { return s == word || s.startsWith(word ~ " "); }

struct Item
{
	string name;
	int level;
	string[] description;

	string lexicographicSummary() const
	{
		auto levelStr = level.format!"%02d";

		foreach (i, line; description)
			foreach (prefix; [
				"Adrena-Time High",
				"Adreno Healing",
				"Alcohol",
				"Caffeine Drink",
				"Carbohydrates",
				"Hardened",
				"Immunized",
				"Insight",
				"Meat",
				"Nicotine High",
				"Sniper",
				"Speedy",
				"Sugary Drink",
			])
				if (line.startsWith(prefix ~ ": "))
					return ("CONSUMABLE" ~ description[i .. $] ~ ["", name, levelStr] ~ description[0 .. i]).join("\n");
		if (description.get(0, "").contains(" Mod - "))
			return ("MOD" ~ description ~ ["", name, levelStr]).join("\n");
		if (description.get(0, "").startsWithWord("Skill"))
			return ("WEAPON" ~ [levelStr, name] ~ description).join("\n");
		if (description.get(0, "").startsWithWord("Armor Type"))
			return ("ARMOR" ~ description[1..$] ~ ["", levelStr, description[0], name]).join("\n");
		return ("OTHER" ~ [levelStr, name] ~ description).join("\n");
	}

	int opCmp(ref const Item b) const
	{
		return cmp(lexicographicSummary(), b.lexicographicSummary());
	}
}

string ocr(V)(V i, int dpi = 300)
{
	// stderr.writeln("OCRing...");
	auto p = pipeProcess(["tesseract", "--dpi", dpi.text, "stdin", "stdout"], Redirect.stdin | Redirect.stdout);
	p.stdin.rawWrite(i.toPBM);
	p.stdin.close();
	auto t = cast(string)readFile(p.stdout);
	enforce(p.pid.wait() == 0, "tesseract failed");
	return t
		.replace("@", "9") // WTF!
	;
}

auto toTextMask(V)(V image)
{
	return image.colorMap!(c => isTextColor(c) ? L8.black : L8.white);
}

bool hasText(V)(V mask) { return mask.h.iota.map!(y => mask.w.iota.map!(x => mask[x, y])).joiner.canFind(L8.black); }

Item readItem(V)(V frame)
{
	scope(failure) frame.colorMap!(c => RGBA(c.r, c.g, c.b, c.a)).toPNG.toFile("fail-item.png");
	auto mask = frame.toTextMask();
	scope(failure) mask.toPNG.toFile("fail-item-mask.png");

	Item item;
	item.name = mask.crop(15, 15, 15 + 400, 15 + 48).ocr().strip();

	enum descriptionX = 15;
	xy_t descriptionY = 227;
	xy_t descriptionW = mask.w - 30;
	enum descriptionH = 45;

	auto levelMask = mask.crop(260, 200, 500, 240);
	if (levelMask.hasText())
	{
		scope(failure) levelMask.toPNG.toFile("fail-level.png");
		auto levelStr = levelMask.ocr().strip();
		if (levelStr != "")
		{
			scope(failure) stderr.writeln([levelStr]);
			enforce(levelStr.skipOver("LEVEL "), "Failed to OCR level");
			item.level = levelStr
				.replace(" ", "") // Weird kerning for "11"
				.to!int;
			descriptionY = 240;
		}
	}

	while (true)
	{
		auto descMask = mask.crop(
			descriptionX,
			descriptionY,
			descriptionX + descriptionW,
			descriptionY + descriptionH,
		);
		auto testMask = descMask.crop(0, 0, 20, descMask.h);
		if (!testMask.hasText())
			break;
		scope(failure) descMask.toPNG.toFile("fail-desc.png");
		item.description ~= descMask.ocr().strip();
		descriptionY += descriptionH;
	}

	return item;
}

enum storageItemX0 = (477+739)/2;
enum storageItemY0 = (513+732)/2;
enum itemW = (1311-477)/(4-1);
enum itemH = (1460-513)/(5-1);
enum storageItemRightDX = 2177-477;

enum storagePagerX0 = 3120-storageItemRightDX;
enum storagePagerY0 = 410;
enum storagePagerX1 = 3284-storageItemRightDX;
enum storagePagerY1 = 465;

void moveMouseOnItem(size_t row, size_t column, bool right)
{
	auto x = storageItemX0 + (column * itemW) + (right * storageItemRightDX);
	auto y = storageItemY0 + (row * itemH);
	moveMouse(x.to!int, y.to!int);
}

Item delegate() readItem(size_t row, size_t column, bool right)
{
	moveMouseOnItem(row, column, right);
	Thread.sleep(600.msecs);

	auto c = getCapture().copy();
	Item item;
	auto t = new Thread({
		scope(failure) c.colorMap!(c => RGBA(c.r, c.g, c.b, c.a)).toPNG.toFile("fail.png");
		auto frameRect = findItemFrame(c);
		if (frameRect is typeof(frameRect).init)
			return;
		auto frame = c.crop(frameRect.tupleof);
		item = readItem(frame);
	}).start();
	return {
		t.join();
		return item;
	};
}

int[2] readPager(V)(V c, bool right)
{
	auto x0 = storagePagerX0 + (right * storageItemRightDX);
	auto y0 = storagePagerY0;
	auto x1 = storagePagerX1 + (right * storageItemRightDX);
	auto y1 = storagePagerY1;
	auto mask = c.crop(x0, y0, x1, y1).toTextMask();
	if (!mask.hasText()) return [0, 1];
	auto s = mask.ocr().strip();
	auto parts = s.findSplit("/");
	enforce(parts[1].length, s);
	return [
		parts[0].to!int - 1, // Convert to 0-based
		parts[2].to!int,
	];
}

enum itemRows = 5;
enum itemColumns = 4;
enum itemsPerPage = itemRows * itemColumns;

void scrollPages(int delta)
{
	auto distance = abs(delta);
	foreach (n; 0 .. distance)
		sendScroll(sign(delta));
	Thread.sleep((1000 + 50 * distance).msecs);
}

Item[] readItems(bool right)
{
	Image!BGRA capture = getCapture();
	auto pager = capture.readPager(right);
	writeln(pager);
	if (pager[0] != 0)
	{
		writeln("Scrolling up...");
		moveMouseOnItem(0, 0, right);
		scrollPages(-pager[0]);
		capture = getCapture();
	}

	auto numPages = pager[1];

	size_t numItems;
	auto items = new Item[numPages * itemsPerPage];
	bool done;

	foreach (page; 0 .. numPages)
	{
		Thread[] threads;
		scope (success) // Join when done but also before going to another page, to ensure the "done" flag is synchronous
			foreach (thread; threads)
				thread.join();

		foreach (n; 0 .. itemsPerPage)
		{
			if (done)
				break;
			(size_t pos) {
				auto page   = pos / itemsPerPage;
				auto row    = pos % itemsPerPage / itemColumns;
				auto column = pos                % itemColumns;
				auto itemPromise = readItem(row, column, right);
				threads ~= new Thread({
					auto item = itemPromise();
					writefln("%d/%d/%d: %s", page, row, column, item.lexicographicSummary.split('\n'));
					if (item is Item.init)
					{
						enforce(page + 1 == numPages, "Empty item on non-last page");
						done = true;
					}
					items[pos] = item;
				}).start();
			}(numItems++);
		}

		if (done)
			break;

		scrollPages(+1);
	}

	while (items.length && items[$-1] is Item.init)
		items = items[0 .. $-1];
	return items;
}

void sortedTransfer(bool leftToRight)
{
	auto source = leftToRight ? false : true;
	auto items = readItems(source);
	auto remainingItems = items.length.iota.array;
	auto itemOrder = items.length.iota.array.sort!((a, b) => items[a] < items[b], SwapStrategy.stable);
	auto currentPage = getCapture().readPager(source)[0];

	foreach (itemIndex; itemOrder)
	{
		stderr.writefln("Moving item %d: %s", itemIndex, items[itemIndex].lexicographicSummary.split("\n"));

		moveMouse(3840 / 2, 2160 / 2);
		Thread.sleep(20.msecs);

		auto pos = remainingItems.indexOf(itemIndex).to!int;
		assert(pos >= 0);

		auto page   = pos / itemsPerPage;
		auto row    = pos % itemsPerPage / itemColumns;
		auto column = pos                % itemColumns;
		moveMouseOnItem(row, column, source);

		if (currentPage != page)
		{
			scrollPages(page - currentPage);
			currentPage = page;
		}

		debug (transfer)
		{
			auto item = readItem(row, column, source);
			enforce(item == items[itemIndex],
				"Expected %s but here is %s".format(items[itemIndex], item));
		}
		sendDoubleClick();

		remainingItems = remainingItems.remove(pos);
		if (pos == remainingItems.length && pos % itemsPerPage == 0)
		{
			// Game will scroll up automatically when removing last item
			currentPage--;
			Thread.sleep(1000.msecs);
		}
	}
	assert(!remainingItems.length);
}

void play()
{
	dpy = getDisplay();
	win = findWindowByName(gameTitle);
	geometry = getWindowGeometry(win);

	sortedTransfer(true);
}

import ae.utils.funopt;
import ae.utils.main;
mixin main!(funopt!play);
