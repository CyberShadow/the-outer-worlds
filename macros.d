
import core.sys.posix.sys.time;
import core.thread.osthread;
import core.time;

import ae.utils.array;

import std.algorithm.comparison;
import std.algorithm.iteration;
import std.algorithm.searching;
import std.array;
import std.exception;
import std.random;
import std.range;
import std.stdio : stderr;

import mylib.uinput;
import mylib.linux.input;
import mylib.linux.input_event_codes;
import mylib.window_listener;

enum gameTitle = "The Outer Worlds  ";
enum keyboardDev = "/dev/input/by-id/usb-TrulyErgonomic.com_Truly_Ergonomic_Computer_Keyboard-event-kbd";

void main()
{
	bool gameIsRunning;
	auto windowListener = new WindowListener((title) {
		auto newValue = title == gameTitle;
		if (newValue != gameIsRunning)
		{
			stderr.writeln(newValue ? "Ready" : "Idle");
			gameIsRunning = newValue;
		}
	});
	scope(exit) windowListener.stop();

	UInputFilter filter;
	bool clicking;

	new Thread({
		while (true)
		{
			if (clicking && gameIsRunning)
			{
				synchronized(filter.writer) { filter.writer.send(EV_KEY, BTN_LEFT, 1); filter.writer.send(EV_SYN, SYN_REPORT); }
				Thread.sleep(20.msecs);
				synchronized(filter.writer) { filter.writer.send(EV_KEY, BTN_LEFT, 0); filter.writer.send(EV_SYN, SYN_REPORT); }
				Thread.sleep(20.msecs);
			}
			else
				Thread.sleep(50.msecs);
		}
	}).start();

	final class MyFilter : UInputFilter
	{
		this() { super(keyboardDev, writer); }

		override void processEvent(ref input_event ev)
		{
			synchronized(writer)
			{
				if (ev.type == EV_KEY && ev.code == KEY_F1)
					clicking = ev.value != 0;
				send(ev);
			}
		}
	}

	filter = new MyFilter;
	filter.run();
}
